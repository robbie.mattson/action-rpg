# Action RPG


I wanted to make mechanics similar to the 2D Zelda games. So I made a playable vertical slice of a
2D Action RPG game system. In this system you can attack enemies with the space bar, get attacked, 
and beating enemies nets your experience points which allows you to level up and increase your damage.
It was a rewarding experience developing this system, and upon reflection I can find it easily implemetable
into other projects. 
